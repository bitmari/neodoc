Version 1.5 (Documentation)
01 March 2018

The DAPP

An agriculture smart farm contract platform built on the NEO blockchain. It serves to lower the barrier for farmers to receive funding for viable agricultural projects in Africa and for contract buyers to also be able to easily fund these projects. 

Gas for the platform will primarily be used for distribution of farming inputs at selected Farm Suppliers. Farmers will redeem this token for farming inputs. The Gas can also be used in other scenarios such as distribution of direct aid in humanitarian crisis. This will be called the BitMari Digital Coupon.

This platform is being built after successfully testing the use-case using bitcoin in 2016/2017 period with over 80 women farmers and 9 suppliers participating.


Entities

The platform has the following entities: The farmer, contract buyer and the oracle. 
- The oracle signals the result different events.
- Farmer : a person who has a farm project they want to do and list it on the platform as a contract with details.
- Contract Buyer : a person who buys a farm contract from anywhere in the world.



The DEMO DAPP

Below is what we want built it is a basic model which we will build from.

STAGES 

## STAGE ONE  :  FARMER REGISTRATION
## STAGE TWO : FARM REGISTRATION
## STAGE THREE : PROJECT REGISTRATION
## STAGE FOUR : CONTRACT BUYER REGISTRATION
## STAGE FIVE : SMART FARM CONTRACT BUYING AND FUNDING 
## STAGE SIX : DIGITAL COUPON REDEEMING/TOKENS ON SUPPLIER POS
## STAGE SEVEN : ROI AND PROJECT CLOSING 

MORE DETAILS

STAGE ONE : FARMER REGISTRATION 
THIS CODE IS TO REGISTER A FARMER ON THE BLOCKCHAIN WITH ABILITY TO ADD/DELETE/QUERY FOR TRACKING

 STAGE TWO : FARM REGISTRATION 
 THIS CODE IS TO REGISTER FARM OWNERSHIP ON THE BLOCKCHAIN WITH ABILITY TO ADD/DELETE/QUERY/TRANSFER OWNERSHIP

 STAGE THREE : FARM PROJECT REGISTRATION 
 THIS CODE IS TO REGISTER FARM PROJECT GIVEN THE FARMER IS ALSO ON THE BLOCKCHAIN AND THEIR FARM IS ALSO REGISTERED  ON THE BLOCKCHAIN WITH ABILITY TO ADD/DELETE/QUERY/TRANSFER OF PROJECT OWNERSHIP

STAGE FOUR : CONTRACT BUYER REGISTRATION 
THIS CODE IS TO REGISTER A DIRECT Buyer ON THE BLOCKCHAIN WITH ABILITY TO ADD/DELETE/QUERY 

 STAGE FIVE : THE BITMARI SMART FARM CONTRACT 
## THIS CODE IS TO CREATE A RECORD BETWEEN The Buyer AND Farmer With Terms of the contract ON THE BLOCKCHAIN WITH ABILITY TO ADD/DELETE/QUERY/TRANSFER
Address 

 STAGE SIX : DIGITAL COUPON USE 
CONTRACT NEEDS TO HAVE ADDRESES AND DIGITAL COUPONS USE
 CHECK BALANCE/TRANSFER BALANCE / SALE FROM POS / TILL BALANCE IS ZERO/ USE GAS / EACH FARMER HAS A BALANCE SHARED WITH BUYER 

STAGE SEVEN : FARM CONTRACT CLOSING AND REPAYMENT TO THE BUYER 
